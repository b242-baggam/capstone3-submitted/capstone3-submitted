import { Row, Col, Card } from 'react-bootstrap';
import { Button } from 'react-bootstrap';

export default function Highlights() {
	return (
		<div className="pb-5">
			<div>
				<Row>
					{/* 1st Card */}
					<Col xs={12} md={4}>
						<Card className="cardHighlight p-3">
					  		<Card.Body>
					    		<Card.Title>
					    			<h2>Best Quality Products</h2>
					    		</Card.Title>
					    		<Card.Text>
									Here, at our ShopLoverMe Website, you can find the best quality products for a very low price. Any product that you need, there is a one stop destination. That is <b>ShopLoverMe.</b>
					    		</Card.Text>
						  	</Card.Body>
						</Card>
					</Col>

					{/* 2nd Card */}
					<Col xs={12} md={4}>
						<Card className="cardHighlight p-3">
					  		<Card.Body>
					    		<Card.Title>
					    			<h2>Best Price</h2>
					    		</Card.Title>
					    		<Card.Text>
									Here at <b>ShopLoverMe</b>, we guarantee you the best quality items at the best price. If you find the same product with price lesser than this, We give it to you for free.
					    		</Card.Text>
						  	</Card.Body>
						</Card>
					</Col>

					{/* 3rd Card */}
					<Col xs={12} md={4}>
						<Card className="cardHighlight p-3">
					  		<Card.Body>
					    		<Card.Title>
					    			<h2>Best Offers ever</h2>
					    		</Card.Title>
					    		<Card.Text>
									Customer Satisfaction is the most important thing for us. So, We always give offers so that customers are always happy. Also, we give free vouchers to our regular customers.
					    		</Card.Text>
						  	</Card.Body>
						</Card>
					</Col>
				</Row>
			</div>
	</div>
	)
}