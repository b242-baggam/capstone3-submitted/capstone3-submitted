import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import CourseCard from '../components/CourseCard';

export default function Home() {

	const data = {
	    title: "ShopLoverMe",
	    content: "Welcome to the best shopping website in India",
	    destination: "/courses",
	    label: ""
	}

	return (
		<Fragment>
			<Banner data={data} />
			<Highlights />
		</Fragment>
	)
}